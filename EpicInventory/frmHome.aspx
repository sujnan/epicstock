﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmHome.aspx.cs" Inherits="EpicInventory.frmHome" MasterPageFile="~/frmMaster.Master"  %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<body >
    <center>
        <div align="center" style="overflow-x:auto;height:400px;width:400px">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Item_Id" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Item_Id" HeaderText="Item_Id" InsertVisible="False" ReadOnly="True" SortExpression="Item_Id" />
                    <asp:BoundField DataField="Item_Name" HeaderText="Item_Name" SortExpression="Item_Name" />
                    <asp:BoundField DataField="Item_UOM" HeaderText="Item_UOM" SortExpression="Item_UOM" />
                    <asp:BoundField DataField="Current_Stock" HeaderText="Current_Stock" SortExpression="Current_Stock" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EpicStock_DBConnectionString %>" SelectCommand="SELECT * FROM [Items_Tb]"></asp:SqlDataSource>
            
        </div>
        <div>
        <asp:Button ID="Button1" runat="server" Text="Transaction History"></asp:Button>
        <asp:Button ID="Button2" runat="server" Text="Edit Item" OnClick="Button2_Click"></asp:Button>
        <asp:Button ID="Button3" runat="server" Text="Add New Item" OnClick="Button3_Click"></asp:Button>
        </div>
            </center>
</body>
</html>


</asp:Content>
