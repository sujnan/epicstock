﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmAddItem.aspx.cs" Inherits="EpicInventory.frmAddItem" MasterPageFile="~/frmMaster.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<body>
        <div align="center">

            <h1> Add New Item to Inventory</h1>
            <table>
                <tr>
                    <td>Item Name : </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>UOM : </td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server" Height="100%" Width="100%">
                            <asp:ListItem>-- Select --</asp:ListItem>
                            <asp:ListItem>Packets</asp:ListItem>
                            <asp:ListItem>Pieces</asp:ListItem>
                            <asp:ListItem>Numbers</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Initial stock : </td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1"> </td>
                    <td class="auto-style1"></td>
                </tr>
                <tr>
                    <td> <asp:Button ID="Button1" runat="server" Text="Insert" OnClick="Button1_Click" /> </td>
                    <td> <input type="reset" value="Clear" /></td>
                </tr>
            </table>
        </div>
</body>
</html>
    </asp:Content>